<?php
	session_start();
	require_once("dbAccess/Dbconfig.php");
	require_once("dbAccess/class.user.php");
	$user = new USER();
	$user->logout();
	header('Location: ./index.php');
	return;
?>
