<?php
	session_start();
	require_once("./miscelaneos/cabeceras.php");
	require_once("search_functions.php");
	require_once("./dbAccess/categories.php");
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<!-- Ajuste a la anchura del dispositivo -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title class= "text-capitalize text-justify">WeBstart</title>
	<!--Hojas de estilo.-->
	<link rel="stylesheet" type="text/css" href="css/bootstrap-theme.css">
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">


	<script type="text/javascript">
		function addToCart(elem) {
			var xhttp = new XMLHttpRequest();
			xhttp.onreadystatechange = function () {
				if (xhttp.readyState == 4 && xhttp.status == 200) {
					//document.getElementById("respuesta").innerHTML = xhttp.responseText;
					if (xhttp.responseText == 1) {
						var val = parseInt($("#shoppingCartBets").text())+1
						$("#shoppingCartBets").text(val.toString())
						alert("Apuesta añadida al carrito");
					} else if (xhttp.responseText == 0) {
						alert("Ya había apostado por ese evento, se ha sumado la cantidad en el carrito");

					}
					window.location="index.php";
				}
			}

			idevent    = $(elem).closest('table').attr("id");
			idbet      = $(elem).attr("id");
			
			inputmoney = $(elem).parent().siblings('.money').find('input[id=val'.concat(idbet).concat(']'));
			money      = inputmoney.val();

			if (isCurrency(money)) {
				dir="shoppingCart_functions.php?method=add&idevent=".concat(idevent).concat("&idbet=").concat(idbet).concat("&money=").concat(money);
				inputmoney.addClass('valid');
				xhttp.open("GET", dir, true);
				xhttp.send();
			} else {
				alert("Error: introduce un número");
				inputmoney.addClass('error');
			}
		}

		function isCurrency(val){
			var regex  = /^\d+(\.\d{1,2})?$/;
			return regex.test(val);
		}
	</script>
	
</head>

<body>
	<!-- header: nombre del sitio, botones de login y registro-->
	<?php do_header_detalleApuesta();?>

	<!--Menu Lateral. Categorias y SubCategorias. -->
	<div class="row">
		<div class="col-sm-8">
			<div class="container">
				<div class="col-sm-offset-1">
					<?php
						$search = new SEARCH();
						$search->printSQLbet($_GET['id']);
					?>
				</div>
			</div>
		</div>
	</div>

	<div id="respuesta"></div>
	
	<?php do_footer(); ?>
	<!-- Bootstrap core JavaScript
	================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
</body>
</html>

