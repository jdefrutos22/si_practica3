<?php
	require_once("./dbAccess/Dbconfig.php");

	function imprimirUsuarioPerfil() {

		$database = new Database();
		$db = $database->dbConnection();
		$conn = $db;

		$stmt = $conn->prepare("SELECT * FROM customers WHERE customerid = :customerid");
		$stmt->bindParam(':customerid', $_SESSION['user_id'], PDO::PARAM_INT);
		$stmt->execute();

		$fetch = $stmt->fetchAll();

		foreach ($fetch as $user) {
			$firstname = $user['firstname'];
			$lastname = $user['lastname'];
			$email = $user['email'];
			$nCuenta = $user['creditcard'];
			$credit = $user['credit'];
			$pais = $user['country'];
			$ciudad = $user['city'];
			$address =  $user['address1'];
			$zip = $user['zip'];

			$nCuentaIni = substr($nCuenta,0,4);
			$nCuentaFin = substr($nCuenta,11,4);
		}
		?>
		<table class="table table-bordered table-striped table-nonfluid">
			<thead>
				<th><?= @$user['firstname']." ".$user['lastname']?></th>
			</thead>
			<tbody>
				<tr><td>email: </td><td><?= @$user['email']?></td></tr>
				<tr><td>N° de Cuenta: </td><td><?= @$nCuentaIni.'****'.$nCuentaFin ?></td></tr>
				<tr><td>Dinero en Cuenta: </td><td><?= @$user['credit']."€"?></td></tr>
				<tr><td>Pais: </td><td><?= @$pais?></td></tr>
				<tr><td>Ciudad: </td><td><?= @$ciudad?></td></tr>
				<tr><td>Direccion: </td><td><?= @$address?></td></tr>
				<tr><td>Zip: </td><td><?= @$zip?></td></tr>
			</tbody>
		</table>
	<?php }
?>