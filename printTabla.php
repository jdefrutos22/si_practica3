
<?php
	//require_once("carritoFuncionalidades.php");


	//EVENTOS
	function printEventsIni() {
	?>
		<table class="table table-bordered table-striped table-nonfluid">
			<thead>
				<tr>
					<th class="col-sm-3">Deporte</th>
					<th class="col-sm-3">Fecha</th>
					<th class="col-sm-4">Evento</th>
				</tr>
			</thead>
			<tbody>
	<?php
	}

	function printEvent($categoria, $fecha, $id, $nombreevento) {
	?>
		<tr>
			<td><?php echo "$categoria"?></td>
			<td><?php echo "$fecha"?></td>
			<td><a href="detalleApuesta.php?id=<?php echo $id;?>"><?php echo $nombreevento;?></a></td>
		</tr>
	<?php
	}

	function printEventsNoMatches() {
	?>
		<p>No hay resultados que mostrar</p>
	<?php
	}

	function printEventsEnd() {
	?>
			</tbody>
		</table>
	<?php
	}



	//APUESTAS DEL CARRITO

	function printCartIni($categoria, $fecha, $nombreevento, $idevent) {
	?>
		<!--
		<div class="bets_event">
			<div class="table-responsive">
				<table class="table table-bordered table-striped table-nonfluid table-hover">
					<thead>
						<tr>
							<th class="col-sm-3">Categoria</th>
							<th class="col-sm-3 CartDetalles">Fecha</th>
							<th class="col-sm-3">Apuesta</th>
							<th class="col-sm-2 CartDetalles">Cuota (€)</th>
							<th class="col-sm-1">Importe (€)</th>
						</tr>
					</thead>
					<tbody>
					<form class="form-horizontal" method="post" action="carritoFuncionalidades.php?method=bet">
					-->

		<div class="bets_event">
			<div>
				<h2><?php echo "$nombreevento"?></h2>
				<h4><?php echo "$categoria"?></h4>
				<h5><?php echo "$fecha"?></h5>
			</div>
			<div class="table-responsive">
				<table id="<?php echo "$idevent";?>" class="table table-bordered table-striped table-nonfluid">
					<thead>
						<tr>
							<th class="col-sm-3">Apuesta</th>
							<th class="col-sm-2">Cuota (€)</th>
							<th class="col-sm-1">Importe (€)</th>
							<th class="col-sm-2"></th>
						</tr>
					</thead>
					<tbody>


	<?php
	}



	function printCartBet($apuesta, $cuota, $importe, $idbet) {
	?>	
	<!--

		<tr>
			<td><?php echo "$categoria"?></td>
			<td class="CartDetalles"><?php echo "$fecha"?></td>
			<td><?php echo "$evento"?></td>
			<td class="CartDetalles"><?php echo round($cuota,2);?></td>
			<td><input id="importe<?= @$id ?>" class="form-control money" type="number" min="0" name="importe<?= @$id ?>" value="<?= @$importe ?>" required></td>
			<td><input id="<?= @$id ?>" class="btn btn-danger" name="submit" value="Eliminar" onclick= "rmOfCart(this)" ></td>
		</tr>
	-->

		<tr>
			<td><?php echo "$apuesta"?></td>
			<td><?php echo round($cuota,2);?></td>
			<td class="money"><?php echo "$importe"?></td>
			<td>
				<input id="<?php require_once("shoppingCart.php"); echo "$idbet" ?>" class="btn btn-danger" type="submit" name="submit" value="x" onclick="rmOfCart(this)">
			</td>
		</tr>

		

	<?php
	}

	function printCartNoMatches() {
	?>
		<p>No hay resultados que mostrar</p>
	<?php
	}

	function printCartEnd() {
	?>
		<!--
					</tbody>
				</table>
				<input class="btn btn-success" type="submit" name="submit" value="Apostar todo"> <br>
				</form>
			</div>
		</div>
		-->
						</tbody>
				</table>
			</div>
		</div>

	<?php
	}

	function printCartButton() {
	?>
		<!--	<br>
		<button id="mostrarDetalles" class="btn btn-info col-sm-offset-0" onclick="mostrarDetallesApuestaCart()"> Detalles</button>
		-->

		<div id="betButton">
			<form class="form-horizontal" method="post" action="shoppingCart_functions.php?method=bet" novalidate="novalidate">
				<div class="form-group">
					<div class="col-sm-3 col-sm-offset-4">
						<input class="btn btn-success" type="submit" name="submit" value="Apostar todo">
					</div>
				</div>
			</form>
		</div>
	<?php
	}

	//DETALLES DE APUESTA
	function printBetDetailsIni($categoria, $fecha, $nombreevento, $idevent) {
	?>
		<div>
			<h2><?php echo "$nombreevento"?></h2>
			<h4><?php echo "$categoria"?></h4>
			<h5><?php echo "$fecha"?></h5>
		</div>
		<div class="table-responsive">
			<table id="<?php echo "$idevent";?>" class="table table-bordered table-striped table-nonfluid">
				<thead>
					<tr>
						<th class="col-sm-4">Apuesta</th>
						<th class="col-sm-2">Cuota (€)</th>
						<th class="col-sm-1">Importe (€)</th>
						<th class="col-sm-1"></th>
					</tr>
				</thead>
				<tbody>
	<?php
	}

	function printBetDetail($apuesta, $cuota, $idbet) {
	?>
		<tr>
			<td><?php echo "$apuesta";?></td>
			<td><?php echo round($cuota,2);?></td>
			<td class="money">
				<input id="<?php echo "val$idbet";?>" type="number" min="0.01" step="0.01" class="form-control">
			</td>
			<td>
				<input id="<?php echo "$idbet";?>" class="btn btn-success" type="submit" name="submit" value="Listo" onclick="addToCart(this)">
			</td>
		</tr>
	<?php
	}

	function printBetDetailsNoMatches() {
	?>
		<p>No hay resultados que mostrar</p>
	<?php
	}

	function printBetDetailsEnd() {
	?>
				</tbody>
			</table>
		</div>
	<?php
	}

	//Tabla de historico.php
	function printHistIni() {
	?>
		<div class="bets_event">
			<div class="table-responsive">
				<table class="table table-bordered table-striped table-nonfluid table-hover" onclick="mostrarDetallesApuestaHist()">
					<thead>
						<tr>
							<th class="col-sm-3">Categoria</th>
							<th class="HistDetalles" class="col-sm-3">Fecha</th>
							<th class="col-sm-3">Apuesta</th>
							<th class="HistDetalles" class="col-sm-2">Cuota (€)</th>
							<th class="col-sm-1">Importe (€)</th>
							<th class="col-sm-2">Ganancia (€)</th>
						</tr>
					</thead>
					<tbody>
	<?php
	}
	


	function printHistBet($categoria, $fecha, $evento, $cuota, $importe) {
	?>
		<tr>
			<td><?php echo "$categoria"?></td>
			<td class="HistDetalles" ><?php echo "$fecha"?></td>
			<td><?php echo "$evento"?></td>
			<td class="HistDetalles"><?php echo round($cuota,2);?></td>
			<td><?php echo round($importe,2);?></td>
			<td class="ganancia"></td>
		</tr>


	<?php
	}

	function printHistEnd() {
	?>
					</tbody>
				</table>
			</div>
		</div>
		<button id="mostrarDetalles" class="btn btn-info col-sm-offset-0" onclick="mostrarDetallesApuestaHist()"> Detalles</button> <br />
	<?php
	}

	//APUESTAS DEL HISTORIAL
	function printHistoricIni() {
	?>
		<div class="table-responsive">
			<table class="table table-bordered table-striped table-nonfluid">
				<thead>
					<tr>
						<th class="col-sm-1">Fecha de evento</th>
						<th class="col-sm-1">Fecha de apuesta</th>
						<th class="col-sm-1">Categoría</th>
						<th class="col-sm-1">Evento</th>
						<th class="col-sm-1">Apuesta</th>
						<th class="col-sm-1">Cuota (€)</th>
						<th class="col-sm-1">Importe (€)</th>
						<th class="col-sm-1">Estado</th>
					</tr>
				</thead>
				<tbody>
	<?php
	}

	function printHistoric($categoria, $fechacerrar, $nombreevento, $apuesta, $cuota, $importe, $fechaapostado, $state) {
	?>
		<tr>
			<td><?php echo "$fechacerrar"?></td>
			<td><?php echo "$fechaapostado"?></td>
			<td><?php echo "$categoria"?></td>
			<td><?php echo "$nombreevento"?></td>
			<td><?php echo "$apuesta"?></td>
			<td><?php echo round($cuota,2);?></td>
			<td><?php echo "$importe"?></td>
			<td><?php echo "$state"?></td> <!-- resolucion de la apuesta -->
		</tr>
	<?php
	}

	function printHistoricNoMatches() {
	?>
		<p>No hay resultados que mostrar</p>
	<?php
	}

	function printHistoricEnd() {
	?>
				</tbody>
			</table>
		</div>
	<?php
	}
