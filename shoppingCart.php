<?php
	session_start();
	require_once("./miscelaneos/cabeceras.php");
	require_once("search_functions.php");
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<!-- Para que se ajuste a la anchura del dispositivo donde se abra-->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title class= "text-capitalize text-justify">WeBstart</title>

	<link rel="stylesheet" type="text/css" href="css/bootstrap-theme.css">
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css"> 
	<link rel="stylesheet" type="text/css" href="css/font-awesome.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" type="text/css" href="css/paginacion.css">

	<script type="text/javascript">
		function rmOfCart(elem) {
			var xhttp = new XMLHttpRequest();
			table     = $(elem).closest('table');
			idevent   = table.attr("id");
			idbet     = $(elem).attr("id");

			xhttp.onreadystatechange = function () {
				if (xhttp.readyState == 4 && xhttp.status == 200) {
					if ($(elem).closest("tr").siblings().length == 0) {
						$(elem).closest("div[class=bets_event]").remove();
					} else {
						$(elem).closest("tr").remove();
					}

					var val = parseInt($("#shoppingCartBets").text())-1
					$("#shoppingCartBets").text(val.toString())
					if (val == 0) {
						$("#betButton").empty();
						$("#betButton").append("No hay resultados que mostrar");
					}
					alert("Apuesta eliminada del carrito");
				}
			}

			dir="shoppingCart_functions.php?method=rm&idevent=".concat(idevent).concat("&idbet=").concat(idbet);
			xhttp.open("GET", dir, true);
			xhttp.send();
		}
	</script>
</head>

<body>
	<?php do_header_carrito(); ?>

	<div class="row">

		<div class="col-sm-8 col-sm-offset-1">
			<?php
				$search = new SEARCH();
				$search->printSQLcart($_SESSION['shoppingCart']);
			?>
			<div>
				<?php if(isset($_SESSION['error'])) { ?>
					<div class="arrow_box"><label style="display: inline;" class="error" for="nombre"> <?= @$_SESSION['error']?> </label></div>
				<?php unset($_SESSION['error']);
				} ?>
			</div>	
		</div>
	</div>

	<div id="respuesta"></div>

	<?php do_footer();?>



	<!-- Bootstrap core JavaScript
	================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
</body>
</html>
