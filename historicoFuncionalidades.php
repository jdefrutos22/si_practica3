<?php
	require_once("./dbAccess/Dbconfig.php");

	function anadirDineroACuenta($dinero, $idUsuario) {
		$database = new Database();
		$db = $database->dbConnection();
		$conn = $db;

		$stmt = $conn->prepare("SELECT credit FROM customers WHERE customerid = :customerid");
		$stmt->bindParam(':customerid', $idUsuario, PDO::PARAM_INT);
		$stmt->execute();

		$fetch = $stmt->fetchAll();

		foreach ($fetch as $credit) {
			$credito = $credit['credit'];
		}


		$stmt = $conn->prepare("UPDATE customers SET credit = :credit WHERE customerid = :customerid");
		$stmt->bindParam(':customerid', $idUsuario, PDO::PARAM_INT);
		$dinero = $dinero+$credito;
		$stmt->bindParam(':credit', $dinero, PDO::PARAM_INT);
		$stmt->execute();
		return;
	}

	if(isset($_REQUEST['method']) && $_REQUEST['method']=="addMoney") {
		anadirDineroACuenta($_REQUEST['anadidoCuenta'], $_REQUEST['user_id']);
		header("location: historico.php");
		return;
	}
?>