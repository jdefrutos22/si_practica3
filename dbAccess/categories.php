
<?php
require_once("dbAccess/Dbconfig.php");

class CATEGORIES {

	private $conn;

	public function __construct() {
		$database = new Database();
		$db = $database->dbConnection();
		$this->conn = $db;
	}

	public function readCategories() {
		$sql = "SELECT categoryname FROM category";
		$stm = $this->conn->query($sql);
		$result = $stm->fetchAll();
		$categorias = [];
		foreach ($result as $category) {
			array_push($categorias, $category['categoryname']);
		}
		return $categorias;
	}
}

?>
