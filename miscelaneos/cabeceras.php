<?php

	function do_header_default() {
	?>
		<!-- header: nombre del sitio, botones de login y registro-->
		<header class="navbar navbar-default navbar-fixed-top">
			<!-- titulo y boton de collapse -->
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#login">
						<span class="glyphicon glyphicon-user"></span>
					</button>

					<a class="navbar-brand" href="search.php?category=nofilter">
						<img src="img/Blose-logo.png" class="img-responsive" alt="Blose">
					</a>

					<button type="button" class="navbar-toggle collapsed pull-left" data-toggle="collapse" data-target="#sidebar">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
				</div>

				<!-- login y registro, cuando colapsa se mostrarán en el botón anterior -->
				<div id="login" class="navbar-collapse collapse navbar-right">
					<a href="shoppingCart.php" class="btn btn-default">
						<span class="glyphicon glyphicon-shopping-cart"></span> <span id="shoppingCartBets"><?php echo $_SESSION['cart'];?></span> <!-- variable de sesion  -->
					</a>

					<a href="regUser.php" class="btn btn-success navbar-btn">Registrate</a>
					<a href="login.php" class="btn btn-primary navbar-btn">Login</a>
				</div>
			</div>
		</header>
	<?php
	}

	function do_header_index() {
	?>
			<!-- header: nombre del sitio, botones de login y registro-->
		<header class="navbar navbar-fixed-top navbar-default">
			<!-- titulo -->
			<div class="container">
				<div class="navbar-header">
					<a class="navbar-brand" href="index.php">We<i id="brandLetter">B</i>start</a>
					<?php if(isset($_SESSION['s_user_name'])) { ?>
						<p> Bienvenido <?= @$_SESSION['s_user_name'] ?> </p>
					<?php } ?>
				</div>

				<!-- login y registro -->
				<div class="navbar-right ">
					<a href="shoppingCart.php" class="btn btn-default navbar-btn glyphicon glyphicon-shopping-cart"> Carrito</a>
					<?php if(isset($_SESSION['s_user_name'])) { ?>
						<a href="historico.php" class="btn btn-default navbar-btn btn-success glyphicon glyphicon-user"> Perfil</a>
						<a href="logout.php" onclick="logout()" class="btn navbar-btn btn-danger">Logout</a>
						<script>
							function logout() {
								alert("Hasta luego!");							
							}
						</script>
					<?php } else { ?>
						
						<a href="login.php" class="btn navbar-btn btn-primary ">Login</a>
						<a href="registro.php" class="btn navbar-btn btn-success ">Registrase</a>
					<?php }?>
					
				</div>
			</div>
		</header>
	<?php
	}

	function do_header_user($user_name) {
	?>
		<!-- header: nombre del sitio, botones de login y registro-->
		<header class="navbar navbar-default navbar-fixed-top">
			<!-- titulo y boton de collapse -->
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed btn btn-success" data-toggle="collapse" data-target="#login">
						<label> <?php echo "$user_name";?> </label>
						<span class="glyphicon glyphicon-user"></span>
					</button>

					<a class="navbar-brand" href="search.php?category=nofilter">
						<img src="img/Blose-logo.png" class="img-responsive" alt="Blose">
					</a>

					<button type="button" class="navbar-toggle collapsed pull-left" data-toggle="collapse" data-target="#sidebar">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
				</div>

				<!-- login y registro, cuando colapsa se mostrarán en el botón anterior -->
				<div id="login" class="navbar-collapse collapse navbar-right">
					<a href="shoppingCart.php" class="btn btn-default">
						<span class="glyphicon glyphicon-shopping-cart"></span> <span id="shoppingCartBets"><?php echo $_SESSION['cart'];?></span> <!-- variable de sesion  -->
					</a>

					<a href="historic.php" class="btn btn-success navbar-btn glyphicon glyphicon-user"> Perfil</a>
					<a href="logout.php" class="btn btn-danger navbar-btn glyphicon glyphicon-off"> Salir</a>
				</div>
				<div id="login" class="navbar-collapse collapse navbar-right">
					<label class="navbar-text"> Saldo actual:</label>
					<label class="navbar-text" id="saldo">
						<?php
							include_once("dbAccess/Dbconfig.php");
							include_once("dbAccess/class.user.php");
							$user = new USER();
							$user->updateBalance();
							echo round($_SESSION['saldo'],2);
						?>
					</label>
					<label class="navbar-text">€</label>
				</div>
				<div id="login" class="navbar-collapse collapse navbar-right">
					<label class="navbar-text"> <?php echo "$user_name";?> </label>
				</div>
			</div>
		</header>
	<?php
	}

	function do_header_login() { ?>
	<!-- header: nombre del sitio, botones de login y registro-->
		<header class="navbar navbar-default navbar-fixed-top">
			<!-- titulo  -->
			<div class="container">
				<div class="navbar-header">
					<a class="navbar-brand" href="index.php">We<i id="brandLetter">B</i>start</a>
				</div>

				<!-- login y registro -->
				<div class="navbar-right">
					<a href="registro.php" class="btn btn-success navbar-btn">Registrase</a>
					<a href="login.php" class="btn btn-primary navbar-btn">Login</a>
				</div>
			</div>
		</header>
	<?php
	}

	function do_header_carrito() { ?>
		<!-- header: nombre del sitio, botones de login y registro-->
		<header class="navbar navbar-fixed-top navbar-default">
			<!-- titulo -->
			<div class="container">
				<div class="navbar-header">
					<a class="navbar-brand" href="index.php">We<i id="brandLetter">B</i>start</a>
					<?php if(isset($_SESSION['s_user_name'])) { ?>
						<p> Bienvenido <?= @$_SESSION['s_user_name'] ?> </p>
					<?php } ?>
				</div>

				<!-- login y registro -->
				<div class="navbar-right ">
					<?php if(isset($_SESSION['s_user_name'])) { ?>
						<a href="historico.php" class="btn btn-default navbar-btn btn-success glyphicon glyphicon-user"> Perfil</a>
						<a href="logout.php" onclick="logout()" class="btn navbar-btn btn-danger">Logout</a>
						<script>
							function logout() {
								alert("Hasta luego!");							
							}
						</script>
					<?php } else { ?>
						<a href="login.php" class="btn navbar-btn btn-primary ">Login</a>
						<a href="registro.php" class="btn navbar-btn btn-success ">Registrase</a>
					<?php }?>
				</div>
			</div>
		</header>
	<?php
	}

	function do_header_detalleApuesta() { ?>
		<header class="navbar navbar-default navbar-fixed-top">
			<!-- titulo-->
			<div class="container">
				<div class="navbar-header">
					<a class="navbar-brand" href="index.php">We<i id="brandLetter">B</i>start</a>
					<?php if(isset($_SESSION['s_user_name'])) { ?>
						<p> Bienvenido <?= @$_SESSION['s_user_name'] ?></p>
					<?php } ?>
				</div>

				<!-- login y registro -->
					<?php if(isset($_SESSION['s_user_name'])) { ?>
						<div class="navbar-right">
							<a href="logout.php" class="btn navbar-btn btn-danger">Logout</a>
						</div>
					<?php } else { ?>
						<div class="navbar-right">
							<a href="login.php" class="btn navbar-btn btn-primary ">Login</a>
							<a href="registro.php" class="btn navbar-btn btn-success ">Registrase</a>
						</div>
					<?php } ?>	
			</div>
		</header>		
	<?php
	}

	function do_header_historico() { ?>
		<!-- header: nombre del sitio, botones de login y registro-->
		<header class="navbar navbar-fixed-top navbar-default">
			<!-- titulo -->
			<div class="container">
				<div class="navbar-header">
					<a class="navbar-brand" href="index.php">We<i id="brandLetter">B</i>start</a>
					<?php if(isset($_SESSION['s_user_name'])) { ?>
						<p> Bienvenido <?= @$_SESSION['s_user_name'] ?> </p>
					<?php } ?>
				</div>

				<!-- login y registro -->
				<div class="navbar-right ">
					<a href="shoppingCart.php" class="btn btn-default navbar-btn glyphicon glyphicon-shopping-cart"> Carrito</a>
					<?php if(isset($_SESSION['s_user_name'])) { ?>
						<a href="logout.php" onclick="logout()" class="btn navbar-btn btn-danger">Logout</a>
						<script>
							function logout() {
								alert("Hasta luego!");							
							}
						</script>
					<?php } else { ?>	
						<a href="login.php" class="btn navbar-btn btn-primary ">Login</a>
						<a href="registro.php" class="btn navbar-btn btn-success ">Registrase</a>>
					<?php }?>	
				</div>
			</div>
		</header>
	<?php
	}

	function do_header_registro() { ?>
		<!-- header: nombre del sitio, botones de login y registro-->
		<header class="navbar navbar-default navbar-fixed-top">
			<!-- titulo -->
			<div class="container">
				<div class="navbar-header">
					<a class="navbar-brand" href="index.php">We<i id="brandLetter">B</i>start</a>
				</div>

				<!-- login y registro, cuando colapsa se mostrarán en el botón anterior -->
				<div class="navbar-right">
					<a href="login.php" class="btn navbar-btn btn-primary ">Login</a>
					<a href="registro.php" class="btn navbar-btn btn-success ">Registrase</a>
				</div>
			</div>
		</header>
	<?php
	}

	function do_lateral_menu() { ?>
		<div class="col-sm-2">
			<ul class="nav nav-pills nav-stacked">
				<?php 
					$categories = new CATEGORIES();
					foreach($categories->readCategories() as $categoria) {?>
						<li><a href="search.php?category=<?php echo $categoria ?>"> <?php echo $categoria ?> </a></li>
				<?php } ?>
			</ul>
		</div>

	<?php
	}
	
	function do_carrousel() { ?>
		<!-- Carousel de imagenes -->
		<div id="myCarousel" class="carousel slide" data-ride="carousel">
			<ol class="carousel-indicators">
		    	<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
		    	<li data-target="#myCarousel" data-slide-to="1"></li>
		    	<li data-target="#myCarousel" data-slide-to="2"></li>
		    	<li data-target="#myCarousel" data-slide-to="3"></li>
		  	</ol>
		  	<div class="carousel-inner" role="listbox">
				<div class="item active">
			    	<img src="./statics/soccer.jpeg" alt="Chania">
				</div>
				<div class="item">
						<img src="./statics/tennis.jpg" alt="Chania">
				</div>
				<div class="item">
						<img src="./statics/basketball.jpg" alt="Chania">
				</div>
			</div>

			  <!-- Left and right controls -->
			<a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
				<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
		 		<span class="sr-only">Previo</span>
		  	</a>
		  	<a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
		    	<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
		    	<span class="sr-only">Siguiente</span>
		  	</a>
		</div>
	<?php 
	}
	
	function do_footer() { ?>
		<footer class="navbar navbar-left navbar-default navbar-fixed-bottom">
			<?php if(strpos($_SERVER['PHP_SELF'], "index.php")!== false) {?>
			<div> Numero de usuarios conectados: <span id="myBanner"><span/> </div>
			<?php } ?>
			<small class="text-muted text-left"><p>Jose Manuel de Frutos Porras. Grupo 1402 Practica 2 SI</p></small>
		</footer>
	<?php
	}

	function do_searchbar() { ?>

		<div class="row">
			<div class="col-sm-6">
				<div class="input-group">
					<div class="input-group-btn search-panel">
						<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
							<span id="search_concept">Filtrar por</span> <span class="caret"></span>
						</button>
						<ul class="dropdown-menu" role="menu">
							<?php 
								$categories = new CATEGORIES();
								foreach($categories->readCategories() as $categoria) {?>
									<li><a href="search.php?category=<?php echo $categoria ?>"> <?php echo $categoria ?> </a></li>
							<?php } ?>
							<li class="divider"></li>
							<li><a href="search.php?category=nofilter">Cualquiera</a></li>
						</ul>
					</div>
					<input id ="searchBar" type="text" class="form-control" name="search" placeholder="Apuesta"><span class="input-group-btn">
							<button type="submit" class="btn btn-default" onclick="enviarASearch()">
								<i class="glyphicon glyphicon-search"></i>
							</button>
						</span>
					<!--
					<form action="search.php" method="post">
						<input type="text" class="form-control" name="search" placeholder="Apuesta" >
						<span class="input-group-btn">
							<button type="submit" class="btn btn-default">
								<i class="glyphicon glyphicon-search"></i>
							</button>
						</span>
					</form> 
				-->
				</div>
			</div>
		</div>
	<br/>
	<?php
	}  

	?>




	<script>

	function enviarASearch() {
		var search = document.getElementById("searchBar").value;
		window.location="search.php?search="+search;
	}

	function banner() {
		var xhttp = new XMLHttpRequest();
		xhttp.onreadystatechange = function() {
			if(this.readyState == 4 && this.status == 200) {
				document.getElementById("myBanner").innerHTML = this.responseText;
			}
		};
		var dir = "./banner.php";
		xhttp.open("GET", dir, true);
		xhttp.send();
	}
	</script>



