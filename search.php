<?php
	session_start();

	if(!isset($_SESSION['page'])) {
		$_SESSION['page'] = 1;
	}
	
	if($_REQUEST['pageNumber'] == "previous") {
		if($_SESSION['page'] == 1) {
			$_SESSION['page']=$_SESSION['pages'];
		} else {
			$_SESSION['page']--;
		}
		header("location:index.php?search={$_SESSION['search']}&category={$_SESSION['category']}&page={$_SESSION['page']}");
	} else if ( $_REQUEST['pageNumber'] == "next") {
		if($_SESSION['page'] == $_SESSION['pages']) {
			$_SESSION['page'] = 1;
		} else {
			$_SESSION['page']++;
		}
		header("location:index.php?search={$_SESSION['search']}&category={$_SESSION['category']}&page={$_SESSION['page']}");
	} else {

		if ($_REQUEST['category'] != "") {
			if ($_REQUEST['category'] == "nofilter") {
				$_SESSION['category'] = "";
			}else if ($_REQUEST['category'] != $_SESSION['category']) {
				$_SESSION['page'] = 1;
				$_SESSION['category']=$_REQUEST['category'];
				$_SESSION['search'] = "";
				header("location:index.php?search={$_SESSION['search']}&category={$_SESSION['category']}&page={$_SESSION['page']}");
				return;
			} 
		} else {
			$_SESSION['category'] = "";
		}

		if ($_REQUEST['search'] != "") {
			if($_REQUEST['search'] != $_SESSION['search']) {
				$_SESSION['page'] = 1;
				$_SESSION['search']=$_REQUEST['search'];
				$_SESSION['category'] = "";
				header("location:index.php?search={$_SESSION['search']}&category={$_SESSION['category']}&page={$_SESSION['page']}");
			}
		} else {
			$_SESSION['search']="";
		}

		header("location:index.php?search={$_SESSION['search']}&category={$_SESSION['category']}&page={$_SESSION['page']}");
	}
	//header("location:index.php?search={$_SESSION['search']}&category={$_SESSION['category']}&page={$_SESSION['page']}");
	return;
?>
