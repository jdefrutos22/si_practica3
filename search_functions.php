<?php
require_once("printTabla.php");
require_once("dbAccess/Dbconfig.php");
//patrones y repeticiones de codigo
$_SESSION['MAX_PAGE_SIZE']=20;

class SEARCH {
	private $conn;

	private function createPattern($string) {
		$lower = strtolower($string);
		$lower = preg_replace("/[ ]+|(vs)/", "|", $lower);
		$patronCat = "/.*(" . $lower . ").*/";
		return $patronCat;
	}

	public function __construct() {
		$database = new Database();
		$db = $database->dbConnection();
		$this->conn = $db;
	}

	public function runQuery($sql) {
		$stmt = $this->conn->prepare($sql);
		return $stmt;
	}

	//Eventos y busquedas en eventos
	public function printSQLevents($page) {
		
		try {
			$offset = ($page-1)*$_SESSION['MAX_PAGE_SIZE'];
			$stmt = $this->conn->prepare("SELECT * FROM bets NATURAL JOIN category WHERE bets.category=category.categoryid ORDER BY betcloses DESC LIMIT :limit OFFSET :offset");
			$stmt->bindParam(':offset', $offset, PDO::PARAM_INT);
			$stmt->bindParam(':limit', $_SESSION['MAX_PAGE_SIZE'], PDO::PARAM_INT);
			$stmt->execute();

			$fetch = $stmt->fetchAll();
			$results = 0;

			foreach ($fetch as $bet) {
				if ($results == 0){
					$results =1;
					printEventsIni();
				}
				printEvent($bet['categoryname'], $bet['betcloses'], $bet['betid'], $bet['betdesc']);
			}

			if ($results == 1) {
				printEventsEnd();
			} else {
				printEventsNoMatches();
			}

			//NUMERO DE PAGINAS
			$stmt = $this->conn->prepare("SELECT count(*) FROM bets NATURAL JOIN category WHERE bets.category=category.categoryid");
			$stmt->execute();

			$row = $stmt->fetch();
			$_SESSION['pages'] = ceil($row['count'] / $_SESSION['MAX_PAGE_SIZE']);
			echo "<div id=\"max_pages\" class=\"hidden\">{$_SESSION['pages']}</div>";

		} catch(PDOException $ex) {
			echo $ex->getMessage();
		}
	}

	public function printSQLevents_searchName($search) {
		$results=0;
		$offset = $_SESSION['page']*$_SESSION['MAX_PAGE_SIZE'];


		try {
			$stmt = $this->conn->prepare("SELECT * FROM bets NATURAL JOIN category WHERE bets.category=category.categoryid AND betdesc LIKE :search ORDER BY betcloses DESC LIMIT :limit");
			$stmt->bindParam(':limit', $offset, PDO::PARAM_INT);
			$search = '%'.$search.'%';
			$stmt->bindParam(':search', $search, PDO::PARAM_STR);
			$stmt->execute();


			$fetch = $stmt->fetchAll();
			$results = 0;

			$count = 0;
			foreach ($fetch as $bet) {
				$count++;
				if ($results == 0){
					$results =1;
					printEventsIni();
				} else if ($count >=$_SESSION['MAX_PAGE_SIZE']*($_SESSION['page']-1)) {
					printEvent($bet['categoryname'], $bet['betcloses'], $bet['betid'], $bet['betdesc']);
				}	
			}

			if ($results == 1) {
				printEventsEnd();
			} else {
				printEventsNoMatches();
			}


			//NUMERO DE PAGINAS
			$stmt = $this->conn->prepare("SELECT count(*) FROM bets WHERE betdesc LIKE :search");
			$stmt->bindParam(':search', $search, PDO::PARAM_STR);
			$stmt->execute();

			$row = $stmt->fetch();
			$_SESSION['pages'] = ceil($row['count'] / $_SESSION['MAX_PAGE_SIZE']);

		} catch(PDOException $ex) {
			echo $ex->getMessage();
		}
	}

	public function printSQLevents_searchCategory($category) {
		$results=0;
		$offset = $_SESSION['page']*$_SESSION['MAX_PAGE_SIZE'];

		try {
			$stmt = $this->conn->prepare("SELECT * FROM bets NATURAL JOIN category WHERE bets.category=category.categoryid AND category.categoryname =:category  ORDER BY betcloses DESC LIMIT :limit");
			$stmt->bindParam(':limit', $offset, PDO::PARAM_INT);
			$stmt->bindParam(':category', $category, PDO::PARAM_STR);
			$stmt->execute();

			$fetch = $stmt->fetchAll();
			$results = 0;

			$count = 0;
			foreach ($fetch as $bet) {
				$count++;
				if ($results == 0){
					$results =1;
					printEventsIni();
				} else if ($count >=$_SESSION['MAX_PAGE_SIZE']*($_SESSION['page']-1)) {
					printEvent($bet['categoryname'], $bet['betcloses'], $bet['betid'], $bet['betdesc']);
				}
				
			}

			if ($results == 1) {
				printEventsEnd();
			} else {
				printEventsNoMatches();
			}


			//NUMERO DE PAGINAS
			$stmt = $this->conn->prepare("SELECT count(*) FROM bets NATURAL JOIN category WHERE bets.category=category.categoryid AND category.categoryname =:category");
			$stmt->bindParam(':category', $category, PDO::PARAM_STR);
			$stmt->execute();

			$row = $stmt->fetch();
			$_SESSION['pages'] = ceil($row['count'] / $_SESSION['MAX_PAGE_SIZE']);

		} catch(PDOException $ex) {
			echo $ex->getMessage();
		}

	}

	public function printSQLevents_searchNameCategory($page, $search, $category) {
		$results=0;
		$xml=readXMLevents();

		$patronCat = $this->createPattern($category);
		$patronNam = $this->createPattern($search);


		foreach($xml->children() as $evento) {
			$sujetonombreCat = strtolower($evento->categoria);
			$sujetonombreNam = strtolower($evento->nombre);

			if (preg_match($patronCat, $sujetonombreCat) && preg_match($patronNam, $sujetonombreNam)) {
				if ($results == 0){
					$results=1;
					printEventsIni();
				}
				$attr = $evento->attributes();
				printEvent($evento->categoria, $evento->fecha, $attr['id'], $evento->nombre);
			}
		}

		if ($results == 1) {
			printEventsEnd();
		} else {
			printEventsNoMatches();
		}
	}

	//Apuestas
	public function printSQLbet($idEvent) {
		try {
			$stmt = $this->conn->prepare("SELECT * FROM optionbet NATURAL JOIN options NATURAL JOIN category NATURAL JOIN bets WHERE categoryid=category AND betid = :betid");
			$stmt->bindParam(':betid', $idEvent, PDO::PARAM_INT);
			$stmt->execute();

			$fetch = $stmt->fetchAll();
			$results = 0;

			foreach ($fetch as $option) {
				if ($results == 0){
					$results =1;
					printBetDetailsIni($option['categoryname'], $option['betcloses'], $option['betdesc'], $option['betid']);
				}

				printBetDetail($option['optiondesc'], $option['ratio'], $option['optionid']);
			}

			if ($results == 1) {
				printBetDetailsEnd();
			} else {
				printBetDetailsNoMatches();
			}
		} catch(PDOException $ex) {
			echo $ex->getMessage();
		}
	}

	//Carrito
	public function printSQLcart(&$shoppingCart) {
		if(isset($_SESSION['user_id']) && $_SESSION['user_id'] != "" && $_SESSION['user_id'] != null) {
			// Caso de pintar desde base de datos
			$stmt = $this->conn->prepare("SELECT categoryname,optiondesc,betcloses,betdesc,betid,ratio,bet,clientbets.optionid
			                              FROM clientbets NATURAL JOIN bets NATURAL JOIN category, options
			                              WHERE clientbets.optionid=options.optionid AND bets.category=category.categoryid
			                              AND orderid = :orderid ORDER BY betid");
			$stmt->bindParam(':orderid', $_SESSION['cart_id'], PDO::PARAM_INT);
			$stmt->execute();

			if ($stmt->rowCount() == 0) {
				printCartNoMatches();
				return;
			}

			$fetch = $stmt->fetchAll();

			$betid = -1;
			foreach ($fetch as $option) {
				if ($betid != $option['betid']){
					if ($betid != -1) {
						printCartEnd();
					}
					$betid = $option['betid'];
					printCartIni($option['categoryname'], $option['betcloses'], $option['betdesc'], $option['betid']);
				}

				printCartBet($option['optiondesc'], $option['ratio'], $option['bet'], $option['optionid']);
			}
			if ($betid != -1) {
				printCartEnd();
			}
			printCartButton();

		} else {
			// Caso de pintar desde memoria
			try {
				if ($shoppingCart['idEvents'] == null) {
					printCartNoMatches();
					return;
				}

				$stmt = $this->conn->prepare("SELECT * FROM optionbet NATURAL JOIN options NATURAL JOIN category NATURAL JOIN bets WHERE categoryid=category AND betid = :betid");

				foreach ($shoppingCart['idEvents'] as $index => $idEvent) {
					$stmt->bindParam(':betid', $idEvent, PDO::PARAM_INT);
					$stmt->execute();

					$fetch = $stmt->fetchAll();

					$results = 0;
					foreach ($fetch as $option) {
						foreach($shoppingCart['idBets'][$index] as &$dupla) {
							if ($dupla[0] == $option['optionid']) {
								if ($results == 0){
									$results = 1;
									printCartIni($option['categoryname'], $option['betcloses'], $option['betdesc'], $option['betid']);
								}

								printCartBet($option['optiondesc'], $option['ratio'], $dupla[1], $option['optionid']);
								break;
							}
						}
					}
					printCartEnd();
				}
			} catch(PDOException $ex) {
				echo $ex->getMessage();
			}
		}
	}

	//Historial
	public function printSQLhistoric() {
		$results=0;
		$offset = $_SESSION['historicPage']*$_SESSION['MAX_PAGE_SIZE'];

		//SELECT categoryname,betcloses,betdesc,optiondesc,ratio,bet from clientbets NATURAL JOIN bets NATURAL JOIN category, options where winneropt=options.optionid AND categoryid=bets.category AND customerid=177;
		try {
			$stmt = $this->conn->prepare("SELECT categoryname,betcloses,betdesc,optiondesc,ratio,bet,date::DATE,winneropt, clientbets.optionid
			                              FROM clientbets NATURAL JOIN clientorders NATURAL JOIN bets NATURAL JOIN category, options
			                              WHERE clientbets.optionid=options.optionid AND categoryid=bets.category
			                                                               AND customerid=:customerid
			                              ORDER BY date DESC LIMIT :limit");
			$stmt->bindParam(':customerid', $_SESSION['user_id'], PDO::PARAM_INT);
			$stmt->bindParam(':limit', $offset, PDO::PARAM_INT);
			$stmt->execute();

			if ($stmt->rowCount() == 0) {
				printHistoricNoMatches();
				return;			
			}

			$fetch = $stmt->fetchAll();
			$results = 0;

			$count = 0;
			foreach ($fetch as $historic) {
				if ($results == 0){
					$results = 1;
					printHistoricIni();
				}
				if ($count >=$_SESSION['MAX_PAGE_SIZE']*($_SESSION['historicPage']-1)) {
					if ($historic['winneropt'] == $historic['optionid']) {
						$state = "Ganado";
					} elseif ($historic['winneropt'] != null) {
						$state = "Perdido";
					} else {
						$state = "No acabada";
					}
					printHistoric($historic['categoryname'], $historic['betcloses'], $historic['betdesc'],
				    					$historic['optiondesc'], $historic['ratio'], $historic['bet'],$historic['date'], $state);
				}
				$count++;
			}

			printHistoricEnd();

		} catch(PDOException $ex) {
			echo $ex->getMessage();
		}
	}
}
?>
