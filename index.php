<?php
	session_start();
	require_once("./miscelaneos/cabeceras.php");
	require_once("search_functions.php");
	require_once("./dbAccess/categories.php");
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<!-- Ajuste a la anchura del dispositivo -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title class= "text-capitalize text-justify">WeBstart</title>
	<!--Hojas de estilo.-->
	
	<link rel="stylesheet" type="text/css" href="css/bootstrap-theme.css">
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css"> 
	<link rel="stylesheet" type="text/css" href="css/font-awesome.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" type="text/css" href="css/paginacion.css">
</head>

<body onLoad="setInterval('banner()',3000);">
	<!-- header: nombre del sitio, botones de login y registro-->

	<?php do_header_index(); ?>

	<!-- Menu Lateral -->
	<div class="row">
		<?php do_lateral_menu(); ?>
		<div class="col-sm-8">
			<div class="container">
				<div class="col-sm-offset-1">
					<?php do_carrousel(); ?>
					<br>
					<?php do_searchbar(); ?>
					<br/>
					<div class="container">
						<div class="row">
							<div class="col-sm-10">
								<!-- Carga dinamica de los datos -->
								<!-- En la posicion 0 estara el identificador unico para los detalles de apuesta -->
								<div id="table_results">
									<?php
										//$_SESSION['searchByCategory'] = false;
										$search = new SEARCH();
										if(isset($_REQUEST['category']) && isset($_REQUEST['search'])) {
											if ($_REQUEST['category']!="") {
												$search->printSQLevents_searchCategory($_REQUEST['category']);
							
											} else if($_REQUEST['search']!="") {
												$search->printSQLevents_searchName($_REQUEST['search']);
											}
										}
									?>

								</div>
								<?php
									if(isset($_REQUEST['category']) || isset($_REQUEST['search']) || isset($_REQUEST['pageNumber'])) {
								?>
								<div class="pagination">
									<a href="#" class="first" data-action="first">&laquo;</a>
									<a href="search.php?pageNumber=previous" class="previous" data-action="previous">&lsaquo;</a>
									<input type="text" readonly="readonly" value="<?= @$_SESSION['page'] ?> / <?= @$_SESSION['pages'] ?>"/>
									<a href="search.php?pageNumber=next" class="next" data-action="next">&rsaquo;</a>
									<a href="#" class="last" data-action="last">&raquo;</a>
								</div>
								<?php } ?>
							</div>
						</div>
					</div>
				</div>	
			</div>
		</div>
	</div>

				

	<?php do_footer(); ?> 
																									
	
	<!-- Bootstrap core JavaScript
	================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/busquedasYfiltros.js"></script>

</body>
</html>
