<?php
	require_once("app/completions/completions.php");
	require_once("search_functions.php");
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<!-- Para que se ajuste a la anchura del dispositivo donde se abra-->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>blose</title>

	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="css/bootstrap-theme.css">
</head>

<body>
	<?php
		do_header_user_historic($_SESSION['s_user_name']);
	?>

	<div class="row">
		<?php do_sidebar(); ?>

		<div class="col-sm-8 col-sm-offset-1">
			<?php
				$search = new SEARCH();
				$search->printSQLhistoric();
			?>
		</div>
	</div>

	<?php do_footer();?>

	<!-- Bootstrap core JavaScript
	================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
</body>
</html>
