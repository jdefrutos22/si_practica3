<?php
	session_start();
	require_once("historicoFuncionalidades.php");
	require_once("usuarioFuncionalidades.php");
	require_once("./miscelaneos/cabeceras.php");
	require_once("search_functions.php");
?>


<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<!-- Ajuste a la anchura del dispositivo -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title class= "text-capitalize text-justify">WeBstart</title>
	<!--Hojas de estilo.-->
	<link rel="stylesheet" type="text/css" href="css/bootstrap-theme.css">
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
</head>

<body>

	<?php  do_header_historico();
	$_SESSION['historicPage'] = 1; //Siempre ensenamos primero las ultimas apuestas
			//NUMERO DE PAGINAS
	$database = new Database();
	$db = $database->dbConnection();
	$conn = $db;
	$stmt = $conn->prepare("SELECT count(*) FROM clientbets WHERE customerid = :customerid AND confirmed=TRUE");
	$stmt->bindParam(':customerid', $_SESSION['user_id'], PDO::PARAM_STR);
	$row = $stmt->fetch();
	$_SESSION['historicPages'] = ceil($row['count'] / $_SESSION['MAX_PAGE_SIZE'])+1;
	?>

	<div class="row">
			<div class="col-sm-7 col-sm-offset-0">
					<?php
						$search = new SEARCH();
						$search->printSQLhistoric();
					?>
				<div class="pagination">
					<a href="historico_paginacion.php" class="first" data-action="first">&laquo;</a>
					<a href="historico_paginacion.php?pageNumber=previous" class="previous" data-action="previous">&lsaquo;</a>
					<input type="text" readonly="readonly" value="<?= @$_SESSION['historicPage'] ?> / <?= @$_SESSION['historicPages'] ?>"/>
					<a href="historico_paginacion.php?pageNumber=next" class="next" data-action="next">&rsaquo;</a>
					<a href="historico_paginacion.php" class="last" data-action="last">&raquo;</a>
				</div>
			</div>
				
			<div class="col-sm-4">
			<?php imprimirUsuarioPerfil() ?>
			<br />
				<form class="form-horizontal" method="post" action="historicoFuncionalidades.php?method=addMoney&user_id=<?= @$_SESSION['user_id']?>">
					<div class="form-group">
						<label class="control-label col-sm-4">Añadir dinero a la cuenta (€): </label>
						<div class="col-sm-3">
							<input class="form-control" type="number" min="0" name="anadidoCuenta" required> 
						</div>
						<div class="col-sm-3">
							<input class="btn btn-success" type="submit" value="Cargar la Cuenta!">
						</div>
					</div>
				</form>
			</div> 
		</div>
	</div>


	<script>
	function mostrarDetallesApuestaHist() {
		$(document).ready(function() {
		     $(".HistDetalles").toggle();
		});
	}
	</script>
	

	<?php do_footer();?>
	<!-- Bootstrap core JavaScript
	================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script src="js/bootstrap.min.js"></script>

</body>
</html>
